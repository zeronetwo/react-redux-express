const webpack = require('webpack')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path')

module.exports = merge(common, {
  entry: {
    index: ['./src/main.js', 'webpack-hot-middleware/client']
  },
  mode: 'development',
  devtool: 'source-map',
  devServer: {
    historyApiFallback: true
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
});